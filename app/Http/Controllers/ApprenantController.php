<?php

namespace App\Http\Controllers;

use App\Models\Apprenants;
use Illuminate\Http\Request;
use App\Models\Villes;
use Exception;

class ApprenantController extends Controller
{
    public function index()
    {
        $villes = Villes::orderBy('name','ASC')->get();
        $apprenants = Apprenants::with('ville')->orderBy('nom')->get();

        return view('forms.form',compact('villes', 'apprenants'));
    }

    public function store(Request $request)
    {

       $request->validate([
            "nom"=>"required",
            "prenom"=>"required",
            "matricule"=>"required|max:3|unique:apprenants,matricule",
            "contact"=> "required",
            "ville"=>"required|exists:villes,id"
        ],
        [
            "nom.required"=>"Le nom est obligatoire.",
            "nom.string"=>"Le nom doit être une chaine de caractères.",
            "matricule.unique"=>"Un enregistrement porte déjà ce matricule"
        ]);

        try{
            
        $apprenant = new Apprenants();

        $apprenant->nom = $request->nom;
        $apprenant->prenom = $request->prenom;
        $apprenant->matricule = $request->matricule;
        $apprenant->ville_id = $request->ville;
        $apprenant->contact = $request->contact;

            $apprenant->save();

        }catch(Exception $e){
            dd($e->getMessage());
        }

        return redirect()->back()->with('success', 'Enregistrement effectué!');
    }

    public function editApprenant($id){
        
        $villes = Villes::orderBy('name','ASC')->get();
        $apprenant = Apprenants::where('id',$id)->first();

        return view('forms.edit-form', compact('villes', 'apprenant'));
    }





    public function updateApprenant(Apprenants $item, Request $request)
    {
        // $check_apprenant = Apprenants::where('id', $id)->first();
        // if($check_apprenant){

            
        $request->validate([
            "nom"=>"required",
            "prenom"=>"required",
            "matricule"=>"required|max:3",
            "contact"=> "required",
            "ville"=>"required|exists:villes,id"
        ],
        [
            "nom.required"=>"Le nom est obligatoire.",
            "nom.string"=>"Le nom doit être une chaine de caractères.",
            "matricule.unique"=>"Un enregistrement porte déjà ce matricule"
        ]);


        $item->nom = $request->nom;
        $item->prenom = $request->prenom;
        $item->contact = $request->contact;
        $item->ville_id = $request->ville;
        $item->matricule = $request->matricule;

        $item->update();

            return redirect()->route('inscription.list')->with('success', 'Modification effectuée!');


        // }else{
        //     return redirect()->back()->with('error', 'erreuur');
        // }
    }
}
