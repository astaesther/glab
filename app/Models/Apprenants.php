<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Apprenants extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "apprenants";

    protected $fillable = [
        "nom","prenom","matricule","ville_id","contact"
    ];

    public function ville()
    {
        return $this->belongsTo(Villes::class);
    }

}
