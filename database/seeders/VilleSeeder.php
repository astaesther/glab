<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Villes;

class VilleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            ['name'=>'Abidjan'],
            ['name'=>'Bouaké'],
            ['name'=>'San Pédro'],
            ['name'=>'Bonoua'],
            ['name'=>'Touba'],
            ['name'=>'Bassam'],
            ['name'=>'Yamoussoukro'],
            ['name'=>'Korogho']
        ];

        Villes::insert($datas);
    }
}
