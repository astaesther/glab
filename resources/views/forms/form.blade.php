<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
<link rel="stylesheet" href="{{asset('font-awesome.css')}}" />

</head>
<body>
    <div class="container">
        <header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
          <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
            <svg class="bi me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
            <span class="fs-3 fw-bolder">G-Lab</span>
          </a>
    
          <ul class="nav nav-pills">
            <li class="nav-item"><a href="{{route('inscription.list')}}" class="nav-link active" aria-current="page">Inscriptions</a></li>
            <li class="nav-item"><a href="#" class="nav-link">Taches</a></li>
            <li class="nav-item"><a href="#" class="nav-link">Pricing</a></li>
            <li class="nav-item"><a href="#" class="nav-link">FAQs</a></li>
            <li class="nav-item"><a href="#" class="nav-link">About</a></li>
          </ul>
        </header>

        <div class="row">
            
            <div class="col-lg-4 col-md-4">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif



                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif


                <form action="{{route('inscription.store')}}" method="post">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <h2>Formulaire</h2>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <label for="nom">Nom</label>
                                        <input  type="text" name="nom" id="nom" class="form-control @error('nom') is-invalid @enderror ">
                                    </div>
                                    @error('nom')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <label for="prenom">Prenom</label>
                                        <input type="text" name="prenom" id="nom" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <label for="contact">Contact</label>
                                        <input type="text" name="contact" id="nom" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <label for="ville">Ville</label>
                                        <select name="ville" id="ville" class="form-select form-control">
                                            <option value="">Choisir une ville</option>
                                            @foreach ($villes as  $ville)
                                            <option value="{{$ville->id}}">{{$ville->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <label for="matricule">Matricule</label>
                                        <input type="text" name="matricule" id="nom" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" value="Enregistrer" class="btn btn-success">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-8 col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h2>Liste des inscriptions</h2>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-responsive table-hover">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Nom</td>
                                    <td>Prenom</td>
                                    <td>Contact</td>
                                    <td>Matricule</td>
                                    <td>Ville</td>
                                    <td>Options</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($apprenants as $key=>$apprenant)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$apprenant->nom}}</td>
                                        <td>{{$apprenant->prenom}}</td>
                                        <td>{{$apprenant->contact}}</td>
                                        <td>{{$apprenant->matricule}}</td>
                                        <td>{{$apprenant->ville->name}}</td>
                                        <td>
                                            <a href="{{route('inscription.edit', $apprenant->id)}}" class="btn btn-success">
                                                <i class="fa fa-solid fa-user"></i>  Modifier      
                                            </a>
                                            <a href="" class="btn btn-danger">
                                                <i class="fa fa-solid fa-user"></i>  Supprimer      
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
<script src="{{asset('font-awesome.min.js')}}"></script>

</body>
</html>